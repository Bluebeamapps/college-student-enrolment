angular.module('tab-enrollment', [])

.controller('EnrollmentController', function($scope, $http, $filter, $timeout) {
    $scope.searchType = 'enrollmentId';
    $scope.searchString = '';
    $scope.isSearching = false;

    $scope.selectedClass = {};
    $scope.selectedDiscipline = {};

    $scope.search = function() {
        var url = 'students/search?'
        + $scope.searchType + '=' 
        + $scope.searchString;

        $scope.isSearching = true;
        
        $http.get(url)
        .then(function(result) {
            //Success.
            $scope.searchResults = result.data.data;
        }, function() {
            //Error.
            $scope.errorMessage = 'Ocorreu um erro ao realizar a consulta';
        })
        .finally(function() {
            //Finally.
            $scope.isSearching = false;
        });
    };

    $scope.onStudentSelected = function(enrollmentId) {
        $scope.selectedEnrollmentId = enrollmentId;

        $scope.selectedClass.class = undefined;
        $scope.selectedDiscipline.discipline = undefined;

        //Fetch data.
        getEnrolledDisciplines();
        getPendingDisciplines();
    };

    $scope.onPendingDisciplineSelected = function(selectedDiscipline) {
        if (selectedDiscipline != null && selectedDiscipline.discipline != null) {
            var selectedDisciplineId = selectedDiscipline.discipline.iddisciplina;
            getClasses(selectedDisciplineId);
        }
    };

    $scope.clearSearch = function() {
        $scope.searchString = '';
        $scope.searchResults = [];
    };

    $scope.closeAlert = function() {
        $scope.successMessage = undefined;
        $scope.errorMessage = undefined;
    };

    $scope.addDiscipline = function() {
        var url = 'students/enroll';
        var data = {
            classId: $scope.selectedClass.class.idturma,
            enrollmentId: $scope.selectedEnrollmentId
        };

        $http.post(url, data).then(function() {
            //Success.
            $scope.successMessage = 'Disciplina adicionada'
            $scope.selectedDiscipline.discipline = undefined;
            $scope.selectedClass.class = undefined;

            getEnrolledDisciplines();
            getPendingDisciplines();
        }, function() {
            //Error.
            $scope.errorMessage = 'Não foi possível adicionar a disciplina'
        });
    };

    function getEnrolledDisciplines() {
        //Prepare the enrollmentId just so it can be placed in the url path.
        var enrollmentId = $scope.selectedEnrollmentId.replace(/\./g, '');

        var url = 'students/' + enrollmentId + '/disciplines?enrolled';

        $http.get(url).then(function(result) {
            //Success.
            $scope.disciplines = result.data.data;
            calculateTotalDisciplineHours($scope.disciplines);
        }, function() {
            //Error.
            $scope.errorMessage = 'Não foi possível buscar as disciplinas';
        })
    }

    function getPendingDisciplines() {
        //Prepare the enrollmentId just so it can be placed in the url path.
        var enrollmentId = $scope.selectedEnrollmentId.replace(/\./g, '');

        var url = 'students/' + enrollmentId + '/disciplines';

        $http.get(url).then(function(result) {
            //Success.
            $scope.pendingDisciplines = result.data.data;
        }, function() {
            //Error.
            $scope.errorMessage = 'Não foi possível buscar as disciplinas';
        });
    }

    function getClasses(selectedDisciplineId) {
        var url = 'classes/discipline/' + selectedDisciplineId;

        $http.get(url).then(function(result) {
            //Success.
            $scope.classes = result.data.data;
        }, function() {
            //Error.
            $scope.errorMessage = 'Não foi possível buscar as turmas.';
        });
    }

    function calculateTotalDisciplineHours(disciplines) {
        var total = 0;

        for (var i = 0; i < disciplines.length; i++) {
            total += disciplines[i].ch_disciplina;
        }

        $scope.totalHours = total;
    }

});