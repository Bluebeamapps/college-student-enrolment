angular.module('tab-register', [])

.controller('RegisterController', function($scope, $http) {
    $scope.name = '';
    $scope.address = '';
    $scope.genderSelect = 'NA';
    $scope.isRegistering = false;
    
    $scope.registerStudent = function() {
        $scope.isRegistering = true;
        
        var url = 'students/register';
    
        var data = {
            name: $scope.name,
            address: $scope.address,
            gender: $scope.genderSelect,
        };
        
        $http.post(url, data)
        .then(function(data) {
            //Success.
            $scope.successMessage = 'Aluno cadastrado com sucesso';
            $scope.name = '';
            $scope.address = '';
            $scope.genderSelect = 'NA';
        }, function(error) {
            //Error.
            $scope.errorMessage = 'Ocorreu um erro ao cadastrar o aluno';
        })
        .finally(function() {
            //Finally.
            $scope.isRegistering = false;
        });
    };
    
    $scope.closeAlert = function() {
        $scope.successMessage = undefined;
        $scope.errorMessage = undefined;
    };
    
});