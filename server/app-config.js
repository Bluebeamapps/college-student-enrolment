module.exports = (function() {
    
    var devSettings = {
        DB_CONNECTION_URL: 'postgres://postgres:1234@localhost/collegedb'
    };
    
    var productionSettings = {
        DB_CONNECTION_URL: 'postgres://postgres:1234@localhost/collegedb'
    };
    
    console.log(process.env.NODE_ENV);

    switch(process.env.NODE_ENV) {
        case 'development': {
            return devSettings;
        }

        case 'production': {
            return productionSettings;
        }

        default:
            return {};
    }
    
})();

//        DB_CONNECTION_URL: 'postgres://postgres:cloud9isawesome@localhost/collegedb'
