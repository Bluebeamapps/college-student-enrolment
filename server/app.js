var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.resolve(__dirname, '../client')));
app.use('/students', require('./app/routes/students/register-student'));
app.use('/students', require('./app/routes/students/search-student'));
app.use('/students', require('./app/routes/students/get-disciplines'));
app.use('/students', require('./app/routes/students/enroll-student'));
app.use('/classes', require('./app/routes/classes/get-class'));

var port = process.env.PORT || 3000;

app.listen(port, process.env.IP || "0.0.0.0", function(){
    console.log('Server listening on port ' + port);
});