var crypto = require('crypto');
var randomString = require('randomstring');

var key = '84211132445632441101009866784293';

module.exports.encrypt = function(message) {
    return encrypt(message);
}

module.exports.decrypt = function(message) {
    return decrypt(message);
}

function encrypt(message) {
    var cipher = crypto.createCipher('aes-256-cbc', key);
    
    cipher.update(new Buffer(message, 'utf-8'));
    
    return cipher.final('base64');
}

function decrypt(message) {
    var decipher = crypto.createDecipher('aes-256-cbc', key)
    
    decipher.update(message, 'base64', 'utf-8');
    
    return decipher.final('utf-8');
}