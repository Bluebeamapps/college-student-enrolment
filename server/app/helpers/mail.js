var nodemailer = require('nodemailer');

module.exports.verificationEmail = function (user, callback) {
    var credentials = 'smtps://bluebeamapps.noreply.chatapp%40gmail.com:qweasd321@smtp.gmail.com'; 
    var transporter = nodemailer.createTransport(credentials);
    
    var activationUrl = 'http://localhost:8080/user/activate?' + 
        'email=' + user.email + 
        '&code=' + user.activationcode ;
    
    var mailOptions = {
        from: 'ChatApp <bluebeamapps.noreply.chatapp@gmail.com>',
        to: user.email,
        subject: 'SurveyMe email verification',
        html: '<h3> Hello ' + user.name + '</h3>' + '</br>' + '<a href=' + activationUrl + '> Confirm your email address</a>'
    };
    
    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {
            console.log(err);
        }

        callback(err);
    });
};

module.exports.passwordResetEmail = function (email, params, callback) {
    var credentials = 'smtps://bluebeamapps.noreply.chatapp%40gmail.com:qweasd321#@!@smtp.gmail.com'; 
    var transporter = nodemailer.createTransport(credentials);
    
    var activationUrl = 'http://localhost:8081/auth/recovery/form?' + 
        'p=' + params;
    
    var mailOptions = {
        from: 'ChatApp <bluebeamapps.noreply.chatapp@gmail.com>',
        to: email,
        subject: 'SurveyMe Password Assistance',
        html: '<h3> Hello </h3>' + '</br>' + '<a href=' + activationUrl + '> Reset your password</a>'
    };
    
    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {
            console.log(err);
        }

        callback(err);
    });
};