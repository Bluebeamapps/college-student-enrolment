var express = require('express');
var async = require('async');
var pg = require('pg');
var router = express.Router();
var serverResponse = require('../../helpers/response');
var connectionString = require('../../../app-config').DB_CONNECTION_URL;

router.get('/discipline/*', function(request, response) {
    async.waterfall([
        async.apply(validateJSON, request),
        findClass
    ], 
    function(result) {
        serverResponse.json(result, response);
    });
});

function validateJSON(request, next) {
    var pathArray = request.path.split('/');
    var disciplineId = pathArray[2];
    
    var params = {
        disciplineId: disciplineId
    };
    
    console.log(params);

    next(null, params);
}

function findClass(params, next) {
    pg.connect(connectionString, function(err, client, done) {
        if (err) {
            done();
            return next(err);
        }
        
        var query = 'SELECT DISTINCT turma.* ' +
            'FROM Turmas turma, Disciplina disciplina ' +
            'WHERE turma.IdDisciplina = $1;';
            
        var queryParams = [params.disciplineId];
        
        client.query(query, queryParams, function(err, result) {
            done();
            if (err) {
                return next(err);
            }

            next({data: result.rows});
        });
    });
}

module.exports = router;