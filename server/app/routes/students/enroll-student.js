var express = require('express');
var async = require('async');
var pg = require('pg');
var router = express.Router();
var serverResponse = require('../../helpers/response');
var connectionString = require('../../../app-config').DB_CONNECTION_URL;

router.post('/enroll', function(request, response) {
    async.waterfall([
        async.apply(validateJSON, request),
        enrollStudent
    ], 
    function(result) {
        serverResponse.json(result, response);
    });
});

function validateJSON(request, next) {
    var json = request.body;

    var isValid = json &&
        json.hasOwnProperty('classId') &&
        json.hasOwnProperty('enrollmentId');
        
    var err;
    
    if (!isValid) {
        err = new Error('Invalid JSON');
    }

    next(err, json);
}

function enrollStudent(json, next) {
    pg.connect(connectionString, function(err, client, done) {
        if (err) {
            done();
            return next(err);
        }
        
        var query = 'INSERT INTO PreInscricoes VALUES ($1, $2);';
        var params = [json.classId, json.enrollmentId];
        
        client.query(query, params, function(err, result) {
            if (err) {
                done();
                return next(err);
            }
            
            var query = 'UPDATE Turmas SET NumeroInscritos = NumeroInscritos + 1 ' + 
                'WHERE IdTurma = $1;';
                
            var params = [json.classId];

            client.query(query, params, function(err, result) {
                if (err) {
                    done();
                    return next(err);
                }
                
                done();
                
                var responseData = {
                    status: 201,
                    message: 'Student was successfully enrolled.'
                };
                
                next(responseData, done, result);
            });
        });
        
    });
}

module.exports = router;