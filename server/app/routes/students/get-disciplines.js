var express = require('express');
var async = require('async');
var pg = require('pg');
var router = express.Router();
var serverResponse = require('../../helpers/response');
var connectionString = require('../../../app-config').DB_CONNECTION_URL;

router.get('/*/disciplines', function(request, response) {
    async.waterfall([
        async.apply(validateJSON, request),
        findStudentDisciplines
    ], 
    function(result) {
        serverResponse.json(result, response);
    });
});

function validateJSON(request, next) {
    var params = {
        enrollmentId: extractEnrollmentIdFromPath(request),
        enrolled: request.query.enrolled
    }

    var isValid = params.enrollmentId &&
        params.hasOwnProperty('enrollmentId');
        
    var err;
    
    if (!isValid) {
        err = new Error('Invalid params');
    }

    next(err, params);
}

function findStudentDisciplines(params, next) {
    pg.connect(connectionString, function(err, client, done) {
        if (err) {
            done();
            return next(err);
        }
        
        var query;

        if (params.enrolled != undefined) {
            //Return the disciplines the student is enrolled on.
            query = 'SELECT * FROM PreInscricoes inscricoes ' +
                'JOIN Turmas turma ON inscricoes.MatriculaAluno = $1 ' +
                'AND inscricoes.IdTurma = turma.IdTurma ' +
                'JOIN Disciplina disciplina ON turma.IdDisciplina = disciplina.IdDisciplina;';
        } else {
            //Return the disciplines the student is not enrolled on yet.
            query = 'SELECT disciplinas.IdDisciplina, disciplinas.NomeDisciplina, disciplinas.CH_Disciplina ' +
                'FROM Disciplina disciplinas ' +
                'WHERE disciplinas.IdDisciplina ' +
                'NOT IN ' +
                    '(SELECT turmas.IdDisciplina FROM Turmas turmas, PreInscricoes inscricoes ' +
                    'WHERE inscricoes.IdTurma = turmas.IdTurma ' +
                    'AND inscricoes.MatriculaAluno = $1);';
        }

        var queryParams = [params.enrollmentId];
        
        client.query(query, queryParams, function(err, result) {
            done();
            if (err) {
                return next(err);
            }

            next({data: result.rows});
        });
    });
}

function extractEnrollmentIdFromPath(request) {
    var pathArray = request.path.split('/');
    var enrollmentId = pathArray[1];
    
    enrollmentId = enrollmentId.slice(0, 4) + '.' + 
        enrollmentId.slice(4, 5) + '.' +
        enrollmentId.slice(5, enrollmentId.length);
        
    return enrollmentId;
}

module.exports = router;