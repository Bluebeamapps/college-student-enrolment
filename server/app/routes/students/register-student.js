var express = require('express');
var async = require('async');
var pg = require('pg');
var router = express.Router();
var serverResponse = require('../../helpers/response');
var connectionString = require('../../../app-config').DB_CONNECTION_URL;

router.post('/register', function(request, response) {
    async.waterfall([
        async.apply(validateJSON, request),
        createStudent
    ], 
    function(result) {
        serverResponse.json(result, response);
    });
});

function validateJSON(request, next) {
    var json = request.body;

    var isValid = json &&
        json.hasOwnProperty('name') &&
        json.hasOwnProperty('address') &&
        json.hasOwnProperty('gender');
        
    var err;
    
    if (!isValid) {
        err = new Error('Invalid JSON');
    }

    next(err, json);
}

function createStudent(json, next) {
    pg.connect(connectionString, function(err, client, done) {
        if (err) {
            done();
            return next(err);
        }
        
        var query = 'SELECT nextval(\'SequenciaMatricula\')';
        
        client.query(query, function(err, result) {
            if (err) {
                return next(err);
            }
            
            var enrollmentId = generateEnrollmentId(result.rows[0].nextval);
            
            query = 'INSERT INTO Aluno VALUES ($1, $2, $3, $4)';
            var params = [enrollmentId, json.name, json.address, json.gender];

            client.query(query, params, function(err, result) {
                if (err) {
                    return next(err);
                }
                
                done();
                
                var responseData = {
                    status: 201,
                    message: 'Student was successfully registered.'
                };
                
                next(responseData, done, result);
            });
        });
        
    });
}

function generateEnrollmentId(sequence) {
    var date = new Date();
    
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    
    var enrollmentId = year;
    
    if (month < 6) {
        enrollmentId += '.1.' + zeroPad(sequence, 6);
    } else {
        enrollmentId += '.2.' + zeroPad(sequence, 6);
    }
    
    return enrollmentId;
}

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}

module.exports = router;