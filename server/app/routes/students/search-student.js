var express = require('express');
var async = require('async');
var pg = require('pg');
var router = express.Router();
var serverResponse = require('../../helpers/response');
var connectionString = require('../../../app-config').DB_CONNECTION_URL;

router.get('/search', function(request, response) {
    async.waterfall([
        async.apply(validateJSON, request),
        findStudentInDatabase,
    ], 
    function(result) {
        serverResponse.json(result, response);
    });
});

function validateJSON(request, next) {
    var params = request.query;

    var isValid = params &&
        (params.hasOwnProperty('name') || 
        params.hasOwnProperty('enrollmentId'));
        
    var err;
    
    if (!isValid) {
        err = new Error('Invalid params');
    }

    next(err, params);
}

function findStudentInDatabase(params, next) {
    pg.connect(connectionString, function(err, client, done) {
        if (err) {
            done();
            return next(err);
        }
        
        var param;
        var where = 'WHERE ';
        
        if (params.hasOwnProperty('name')) {
            where += 'NomeAluno LIKE $1';
            param = '%' + params.name + '%';
        } else {
            where += 'MatriculaAluno LIKE $1';
            param = '%' + params.enrollmentId + '%';
        }
        
        var sql = 'SELECT NomeAluno, MatriculaAluno FROM Aluno ' + where;

        client.query(sql, [param], function(err, result) {
            done();
            
            if (err) {
                return next(err);
            }

            next({data: result.rows});
        });
    });
}


module.exports = router;