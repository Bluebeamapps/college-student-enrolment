DROP TABLE Disciplina CASCADE;
DROP TABLE Aluno CASCADE;
DROP TABLE Turmas CASCADE;
DROP TABLE PreInscricoes CASCADE;
DROP SEQUENCE SequenciaMatricula;

CREATE TABLE Disciplina (
IdDisciplina SERIAL PRIMARY KEY,
NomeDisciplina text,
CH_Disciplina int
);

CREATE TABLE Aluno (
MatriculaAluno text PRIMARY KEY NOT NULL,
NomeAluno text,
EnderecoAluno text,
SexoAluno text CHECK (SexoAluno = 'M' OR SexoAluno = 'F')
);

CREATE TABLE Turmas (
IdTurma text PRIMARY KEY,
Sala int,
IdDisciplina int,
HoraInicio time,
HoraFim time,
NumeroInscritos int,
FOREIGN KEY (IdDisciplina) REFERENCES Disciplina(IdDisciplina)
);

CREATE TABLE PreInscricoes (
IdTurma text,
MatriculaAluno text,
FOREIGN KEY (IdTurma) REFERENCES Turmas(IdTurma),
FOREIGN KEY (MatriculaAluno) REFERENCES Aluno(MatriculaAluno),
PRIMARY KEY (IdTurma, MatriculaAluno)
);

CREATE SEQUENCE SequenciaMatricula START 1;

--INSERT INTO Aluno VALUES ('2016.1.000001', 'Marcus Vinicius', 'M');
--INSERT INTO Aluno VALUES ('2016.1.000002', 'Heitor Marques', 'M');
--INSERT INTO Aluno VALUES ('2016.1.000003', 'Bruno Aguiar', 'M');
--INSERT INTO Aluno VALUES ('2016.1.000004', 'Luciana Soares', 'F');

INSERT INTO Disciplina VALUES (DEFAULT, 'Matemática', 40); -- 1
INSERT INTO Disciplina VALUES (DEFAULT, 'Algortimos', 40); -- 2
INSERT INTO Disciplina VALUES (DEFAULT, 'Teoria Geral de Sistemas', 80); -- 3
INSERT INTO Disciplina VALUES (DEFAULT, 'Lógica Matemática', 40); -- 4
INSERT INTO Disciplina VALUES (DEFAULT, 'Banco de Dados', 80); -- 5

INSERT INTO Turmas VALUES ('001N1', 503, 3, '18:35:00', '20:15:00', 0);
INSERT INTO Turmas VALUES ('001N2', 305, 3, '20:35:00', '22:15:00', 0);
INSERT INTO Turmas VALUES ('001N3', 503, 2, '20:35:00', '22:15:00', 0);
INSERT INTO Turmas VALUES ('001N4', 305, 5, '18:35:00', '20:15:00', 0);
INSERT INTO Turmas VALUES ('001N5', 400, 1, '18:35:00', '20:15:00', 0);
INSERT INTO Turmas VALUES ('001N6', 400, 4, '20:35:00', '22:15:00', 0);
INSERT INTO Turmas VALUES ('001N7', 506, 5, '18:35:00', '20:15:00', 0);

--INSERT INTO PreInscricoes VALUES ('001N1', '2016.1.000001');
--INSERT INTO PreInscricoes VALUES ('001N5', '2016.1.000003');


