-- Retorna disciplina em quais o aluno ja foi inscrito.
SELECT * FROM PreInscricoes inscricoes 
JOIN Turmas turma ON inscricoes.MatriculaAluno = '2016.1.000001' AND inscricoes.IdTurma = turma.IdTurma 
JOIN Disciplina disciplina ON turma.IdDisciplina = disciplina.IdDisciplina;

-- Retorna disciplina em quais o aluno ja foi inscrito (SEM JOIN).
SELECT DISTINCT disciplinas.NomeDisciplina, disciplinas.CH_Disciplina 
FROM Disciplina disciplinas, Turmas turmas, PreInscricoes inscricoes 
WHERE inscricoes.MatriculaAluno = '2016.1.000001' 
AND turmas.IdTurma = inscricoes.idTurma 
AND disciplinas.IdDisciplina = turmas.IdDisciplina;

-- Retorna disciplinas em quais o aluno nao foi inscrito ainda.
SELECT disciplina.IdDisciplina, disciplinas.NomeDisciplina, disciplinas.CH_Disciplina 
FROM Disciplina disciplinas
WHERE disciplinas.IdDisciplina 
NOT IN 
    (SELECT turmas.IdDisciplina FROM Turmas turmas, PreInscricoes inscricoes 
     WHERE inscricoes.IdTurma = turmas.IdTurma 
     AND inscricoes.MatriculaAluno = '2016.1.000001');

-- Retorna turmas associadas a uma certa disciplina.
SELECT DISTINCT turma.*
FROM Turmas turma, Disciplina disciplina 
WHERE turma.IdDisciplina = 3;

-- Adiciona pre inscricao.
INSERT INTO PreInscricoes VALUES ('001N1', '2016.1.000001');

-- Incrementa contador de inscritos em uma turma.
UPDATE Turmas SET NumeroInscritos = NumeroInscritos + 1 
WHERE IdTurma = '001N1';

